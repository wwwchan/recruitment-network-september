/**
 * eslint config
 * ESlint配置规则：http://eslint.cn/docs/rules/
 * AlloyTeam ESLint 规则
 * https://github.com/AlloyTeam/eslint-config-alloy
 * 在线浏览规则描述及示例：https://alloyteam.github.io/eslint-config-alloy/
 */
module.exports = {
    root: true,
    env: {
        node: true
    },
    'extends': [
        // 'plugin:vue/essential',
        // '@vue/standard',
        'eslint-config-alloy/vue'
    ],
    globals: {
        '@': false,
        '~V': false,
        '~C': false,
        '~A': false
    },
    rules: {
        // @fixable 一个缩进必须用两个空格替代
        'indent': ['error', 2],
        'no-multi-spaces': [2, { exceptions: { "VariableDeclarator": true } }],
        "vue/no-parsing-error": [2, { "x-invalid-end-tag": false }]
    },
    parserOptions: {
        parser: 'babel-eslint'
    }
}