# Demo System

演示系统

### 克隆项目
```
git clone http://192.168.4.139:10088/zqfront/demo-system.git 项目名称
```

### 安装依赖包并运行
```
// 安装项目依赖
npm i
// 启动本地服务
npm run serve
```
安装项目依赖时，`node-sass`可能会安装不成功，可以使用`npm i node-sass`再次安装一下。

### 编译打包
```
npm run build
```