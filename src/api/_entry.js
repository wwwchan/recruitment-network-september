/**
 * api 入口
*/
export const apiEntry =  {
	dictCat:'dictCat', //字典类
	dict: 'dict', //字典
	boardCat: 'boardCat', //资讯类
	board: 'board', //资讯
	resume: 'resume', //简历
	company: 'company', //招聘企业
	post: 'post', //招聘职位
	postApply: 'postApply', //职位申请
	postCollect: 'postCollect', //职位收藏
	resumeCollect: 'resumeCollect', //简历收藏
	interviewInvite: 'interviewInvite' //面试邀请
}
