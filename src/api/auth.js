//import { httpClient } from "@/common/http";
import { post, get } from '@/api/request'
export default {

    /**
     * 用户登录
     * @param data {
     *   phone: '',
     *   password: ''
     * }
     * @returns  X-Access-Token
     */
    login: (data) => post("/api/sys/login", data),

    /**
     * 用户注册
     * @param data {
     *   userType: 1|2,
     *   phone: '',
     *   password: ''
     * }
     */
    register: (data) => post("/api/sys/user/register", data),

    wxLogin: (wxCode) => post("wx/login", { code: wxCode }),

    wxRegister: (data) => post("/api/wx/register", data),

    /**
     * 登录用户更改自己的密码
     * @param data: {
     *   newPwd: '' //新密码
     * }
     */
    changePwd: (data) => post('/api/sys/changeMyPassword', data),

    /**
     * 获取验证码
     * @param key: 随机数
     * @returns {Promise | Promise<unknown>}
     */
    captcha: (key) => get(`/api/sys/randomImage/${key}`),

    /**
     * 验证手机唯一性
     * @param phone
     * @returns {Promise | Promise<unknown>}
     */
    checkPhone: (phone) => get(`/api/sys/user/checkUserToken?phone=${phone}`),

    /**
     * 验证验证码
     * @param data: {
     *   key: ''
     *   captcha: ''
     * }
     */
    checkCaptcha: (data) => post('/api/sys/checkCaptcha', data),


}