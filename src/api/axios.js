import axios from 'axios';
import qs from 'qs';
import { _API2_ } from '~P/API_config';
axios.defaults.headers = {
    // 'Content-Type': 'application/json;charset=utf8'
    'Content-Type': 'application/x-www-form-urlencoded'
};

/**
 * axios全局配置请求拦截
 */
// 添加请求拦截器
axios.interceptors.request.use(config => {
    // 处理请求数据
    if (window.localStorage.token) {
        config.headers.token = window.localStorage.token;
    }
    config.data = qs.stringify(config.data)
    return config;
}, error => {
    // 请求错误处理
    return Promise.reject(error);
});



// 添加响应拦截器
axios.interceptors.response.use(response => {
    // 处理响应数据
    if (response) {
        switch (response.data.code) {
            case 102:
                // 返回 102 清除token信息并跳转到登录页面      
                window.location.href = _API2_;
                break;
        }
    }
    return response;
}, error => {
    // window.localStorage.clear();
    // window.sessionStorage.clear();
    // 响应错误处理
    return Promise.reject(error.response.data);
});

export default axios;