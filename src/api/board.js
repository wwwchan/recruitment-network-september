import { ApiUtils } from './utils';
import { apiEntry } from "./_entry";

/**
 * 新闻资讯
 */
export default {
    /**
     * 资讯列表®
     */
    boards: (cat, options) => ApiUtils.getByQuery(apiEntry.board, { cat }, {
        fields: "title,brief,titleImages,_createTime",
        sort: '-_createTime',
        ...options || {}
    }),

    /**
     * 资讯详情
     */
    board: (id) => ApiUtils.getById(apiEntry.board, id),

    /**
     * 资讯分类列表
     */
    boardCats: (query, options) => ApiUtils.getByQuery(apiEntry.boardCat, query, {
        pageSize: 100,
        ...options || {}
    }),

}