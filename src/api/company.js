import { ApiUtils } from './utils';
import { apiEntry } from "./_entry";

/**
 * 招聘企业
 */
export default {
    /**
     * 公司详情
     */
    company: (id) => ApiUtils.getById(apiEntry.company, id),

    /**
     * 公司列表
     */
    companies: (pageNo, query, options) => ApiUtils.getByQuery(apiEntry.company, query, {
        pageNo: pageNo,
        pageSize: 10,
        ...options || {}
    }),

    /**
     * 公司发布的职位
     */
    posts: (companyId, options) => ApiUtils.getByQuery(apiEntry.post, { employerId: companyId }, options)
}