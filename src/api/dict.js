import { apiEntry } from "./_entry";
import { ApiUtils } from "@/api/utils";
import { post, get } from '@/api/request'

export default {

    dict: (dicCatKey) => get(`/api/${apiEntry.dict}/${dicCatKey}`),

    dictCats: (query, options) => ApiUtils.getByQuery(apiEntry.dictCat, query, {
        pageSize: 100,
        ...options || {}
    }),

}