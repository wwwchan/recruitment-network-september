export {authApi} from "./auth";
export {dictApi} from "./dict";
export {boardApi} from "./board";
export {companyApi} from "./company";
export {postApi} from "./post";
export {resumeApi} from "./resume";
export {myApi} from "./my";
