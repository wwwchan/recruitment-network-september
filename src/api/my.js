import { ApiUtils } from './utils';
import { apiEntry } from "./_entry";

/**
 * '我的'信息
 *  登录用户权限， header中必须包含登录后获取的 X-Access-Token
 */
export default {

    /**
     * 我的简历
     */
    resume: () => ApiUtils.getMy(apiEntry.resume),

    /**
     * 保存简历
     */
    saveResume: (resume) => ApiUtils.saveMy(apiEntry.resume, resume),

    // '我的'职位收藏
    /**
     * 已收藏职位列表
     */
    postsCollected: () => ApiUtils.getMy(apiEntry.postCollect),

    /**
     * 收藏职位
     */
    collectPost: (postId) => ApiUtils.saveMy(apiEntry.postCollect, { postId }),

    /**
     * 取消职位收藏
     */
    deleteCollectedPost: (postId) => ApiUtils.deleteMy(apiEntry.postCollect, { postId }),


    // '我的'职位申请
    /**
     * 已申请职位
     */
    postsApplied: () => ApiUtils.getMy(apiEntry.postApply),

    /**
     * 申请职位
     */
    applyPost: (postId) => ApiUtils.saveMy(apiEntry.postApply, { postId }),

    /**
     * 取消职位申请
     */
    deleteAppliedPost: (postId) => ApiUtils.deleteMy(apiEntry.postApply, { postId }),


    // '我的'简历收藏
    /**
     * 已收藏简历列表
     */
    resumesCollected: () => ApiUtils.getMy(apiEntry.resumeCollect),

    /**
     * 收藏简历
     */
    collectResume: (resumeId) => ApiUtils.saveMy(apiEntry.resumeCollect, { resumeId }),

    /**
     * 取消简历收藏
     */
    deleteCollectedResume: (resumeId) => ApiUtils.deleteMy(apiEntry.resumeCollect, { resumeId }),


    // '我的'面试邀约
    /**
     * 已邀约面试列表
     */
    interviews: () => ApiUtils.getMy(apiEntry.interviewInvite),

    /**
     * 邀约面试
     */
    inviteInterview: (resumeId) => ApiUtils.saveMy(apiEntry.interviewInvite, { resumeId }),

    /**
     * 取消面试邀约
     */
    deleteInvitedInterview: (resumeId) => ApiUtils.deleteMy(apiEntry.interviewInvite, { resumeId }),

}