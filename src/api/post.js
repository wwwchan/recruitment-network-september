import { ApiUtils } from './utils';
import { apiEntry } from "./_entry";

/**
 * 招聘职位
 */
export default {
    /**
     * 岗位列表
     */
    posts: (query, options) => ApiUtils.getByQueryList(apiEntry.post, query, {
        //zone: "4201",
        fields: 'label,city,employerId,salMonthMax,salMonthMin,welfare',
        sort: '-_createTime',
        ...options || {}
    }),

    /**
     * 岗位详情
     */
    post: (id) => ApiUtils.getById(apiEntry.post, id),

}