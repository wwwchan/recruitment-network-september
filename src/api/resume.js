import { ApiUtils } from './utils';
import { apiEntry } from "./_entry";

/**
 * 求职简历
 * 登录用户权限， header中必须包含登录后获取的 X-Access-Token
 */
export default {
    /**
     * 简历列表
     */
    resumes: (query, options) => ApiUtils.getByQuery(apiEntry.resume, query, {
        sort: '-_createTime',
        ...options || {}
    }),

    /**
     * 简历详情
     */
    resume: (id) => ApiUtils.getById(apiEntry.resume, id),

}