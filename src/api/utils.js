//import appConfig from "@/config/app";

//import { httpClient } from "@/common/http";
import { post, get } from '@/api/request'
const apiKey = '/api/hr';

export const ApiUtils = {

    getById: (entry, id) => get(`${apiKey}/${entry}/${id}`),

    /**
     *
     * @param entry
     * @param query: {
     *   fieldName: 'val'  //查询条件，按字段值查询
     * }
     * @param options {
     *     pageNo: 1,  //第几页
     *     pageSize: 10,  //每页条数
     *     fields: 'a,b,c', //返回字段
     *     sort: 'a,-b'   //排序方式，- 为倒序
     * }
     */
    getByQuery: (entry, query = {}, options) => post(`${apiKey}/${entry}/page`, {
        query,
        ...(options || {}),
    }, { dataPath: 'result.records' }),
    getByQueryList: (entry, options) => post(`${apiKey}/${entry}/page`, {
        ...(options || {}),
    }, { dataPath: 'result.records' }),

    /**
     *
     * @param entry
     * @param data：{}, 包含id即为更新，没有id即为新建
     * @returns {Promise | Promise<unknown>}
     */
    save: (entry, data) => post(`${apiKey}/${entry}`, data),

    deleteById: (entry, id) => delete(`${apiKey}/${entry}`, id),

    /**
     *
     * @param entry
     * @param query: {} 删除条件
     * @returns {Promise | Promise<unknown>}
     */
    delete: (entry, query) => httpClient.delete(`${apiKey}/${entry}`, query),

    getMy: (entry) => get(`${apiKey}/${entry}/my`),

    /**
     *
     * @param entry
     * @param data: {} 包含id即为更新，没有id即为新建
     * @returns {Promise | Promise<unknown>}
     */
    saveMy: (entry, data) => post(`${apiKey}/${entry}/my`, data),

    /**
     *
     * @param entry
     * @param data:{} 包含id即为更新，没有id即为新建
     * @returns {Promise | Promise<unknown>}
     */
    deleteMy: (entry, data) => post(`${apiKey}/${entry}/my`, data)

};