function jgMyAlert(jgMyMessage) {
    var div = document.createElement("div");
    div.innerHTML = "<style type=\"text/css\">" +
        ".jgMyAlert{position: fixed;font-size:12px;z-index:9999999999999;left: 0;right: 0;margin: auto;width: 300px;top: 35vh;padding: 28px 0;text-align: center;border-radius: 3px;background-color: #fff;border: 1px solid #ccc;box-shadow: 0 1px 3px #003B50;}.jgMyMessage{margin-bottom: 20px;font-size:14px}.jgMyClose{padding: 5px 25px;background-color: #1a6bba;color: #fff;font-size: 14px;border-radius: 3px;cursor: pointer;}" +
        "</style>" +
        "<div class=\"jgMyAlert\"><p class=\"jgMyMessage\"></p><a class=\"trackZx jgMyClose\" title=\"提示框关闭按钮\" onclick=\"jgMyClose()\">确定</a></div>";
    document.body.appendChild(div);
    document.querySelector('.jgMyMessage').innerHTML = jgMyMessage;
}
jgMyClose = function() {
    document.querySelector('.jgMyAlert').parentNode.removeChild(document.querySelector('.jgMyAlert'));
};

$('.save').click(function() {
    //姓名
    var name = $(".name").val();
    //联系方式
    var tel = $(".number").val();
    //身份证号
    var id = $(".idcard").val();
    //性别
    var sex = $("#gender_male").val();
    var sex = $("#gender_famale").val();
    //出生日期
    var borntime_year = $('.borntime_year').val();
    var borntime_month = $('.borntime_month').val();
    //参加工作时间
    var worktime_year = $('.worktime_year').val();
    var worktime_month = $('.worktime_month').val();
    //户口所在地
    var from = $('.from').val();
    //现居住城市
    var city = $('.city').val();
    //电子邮箱
    var mail = $('.mail').val();
    //工作地点
    var location = $('.expect-location').find('option:selected').val();

    //期望职业
    var job = $('.expect_job').find('option:selected').val();
    //期望行业
    var work = $('.expect-work').find('option:selected').val();
    //期望月薪
    var earn = $('.earn').find('option:selected').val();
    //工作状态
    var now = $('.now').find('option:selected').val();
    //工作时间
    var beforeworktime_year = $('.beforeworktime_year').val();
    var beforeworktime_month = $('.beforeworktime_year').val();

    var afterworktime_year = $('.afterworktime_year').val();
    var afterworktime_month = $('.afterworktime_year').val();

    //公司名称
    var experience_name = $('.experience_name').val();
    //行业类别
    var experience_type = $('.experience_type').val();
    //职位名称
    var experience_job = $('.experience_job').val();
    //学校名称
    var educational_name = $('.educational_name').val();
    //专业名称
    var educational_part = $('.educational_part').val();
    //学历
    var educational_level = $('.educational_level').val();
    if (name == '') {
        jgMyAlert('您没有填写姓名');
        return false;
    }
    if (tel == '') {
        jgMyAlert('您没有填手机号');
        return false;
    }
    if (id == '') {
        jgMyAlert('您没有身份证号');
        return false;
    }
    var partten = /^1[3,4,5,6,7,8,9]\d{9}$/;
    if (!partten.test(tel)) {
        jgMyAlert('请输入正确的手机号码');
        return false;
    }
    $.ajax({
        type: "post",
        url: "https://hr.hbsrcfwj.cn/api/hr/board/page",
        dataType: 'json',
        data: {
            phone: tel,
            name: name,
            id: id,
            gender: sex,
            //出生年
            birthday: borntime_year,
            //出生月
            //			borntime_month: borntime_month,
            worktime_year: worktime_year,
            worktime_month: worktime_month,
            nativeCity: from,
            city: city,
            email: mail,

            //求职意向
            cityExpected: location,
            industryExpected: job,
            postExpected: work,
            salaryExpected: earn,
            status: now,

            //工作经验
            //工作时间
            //			beforeworktime_year: beforeworktime_year,
            //			beforeworktime_month: beforeworktime_month,
            //			afterworktime_year: afterworktime_year,
            //			afterworktime_month: afterworktime_month,
            company: experience_name,
            title: experience_type,
            experience_job: experience_job,

            //教育背景
            school: educational_name,
            grade: educational_part,
            degree: educational_level

        },
        cache: false,
        success: function(data) {
            jgMyAlert("提交成功!");
        }
    });
});
//getId();
//获取总列表信息
function getId() {
    $.ajax({
        type: "post",
        dataType: "json",
        url: "https://hr.hbsrcfwj.cn/api/hr/boardCat/page",
        //url: "https://hr.hbsrcfwj.cn/api/hr/board/page",
        headers: {
            "Content-Type": "application/json;charset=utf-8"
        },

        data: JSON.stringify({
            // "query": {
            // 	"fieldName": 'djfw',
            // },
            "pageSize": 100,
            "pageNo": 1,
            "sort": "-_createTime",
            //"fields": "title,titleImages,_createTime"
        }),
        //							data:JSON.stringify(),
        //			headers: { "Content-Type": "application/x-www-form-urlencoded" },
        //请求成功
        success: function(data) {
            if (data.code == 200) {
                console.log(data)
                for (var i = 0; i < data.result.length; i++) {
                    $('#news').attr('data-id', data.result['3'].id).attr('data-code', data.result['3'].code);
                }
            }
        },
        error: function() {
            console.log('error')
        }
    });
    $.ajax({
        type: "post",
        dataType: "json",
        //url: "https://hr.hbsrcfwj.cn/api/hr/board/page",
        url: "https://hr.hbsrcfwj.cn/api/hr/boardCat/page",
        headers: {
            "Content-Type": "application/json;charset=utf-8"
        },

        data: JSON.stringify({
            "query": {
                "cat": did,
            },
            "pageSize": 10,
            "pageNo": 1,
            "sort": "-_createTime",
            "fields": "title,titleImages,_createTime"

        }),
        //							data:JSON.stringify(),
        //			headers: { "Content-Type": "application/x-www-form-urlencoded" },
        //请求成功
        success: function(data) {}
    })

}
//getBoard
//获取页面二级分类列表
function getBoard(id) {

    var did = id;
    //公司介绍
    if (id == 'zcfg' || id == 'gsxw' || id == 'zcfx' || id == 'szyw' || id == 'dwbk' || id == 'xxyd') {
        did = 'xwzx';
    }
    //雇员服务/灵活用工
    if (id == 'wbfw' || id == 'rsdlfw' || id == 'lhyg') {

        did = 'lwpq';
    }
    //雇员服务/招聘服务
    if (id == 'zpfw' || id == 'dddrpofw' || id == 'zgdrclt' || id == 'fxqdgl' || id == 'xyzp' || id == 'dzfw') {

        did = 'daszhfw';
    }

    //雇员服务/档案服务
    if (id == 'dafw' || id == 'dazlfw') {

        did = 'daszhfw';
    }

    //雇员服务/培训服务页面
    if (id == 'pxfwym' || id == 'pxxm' || id == 'fwnrjlc') {

        did = 'daszhfw';
    }

    //党建服务/办事流程介绍
    if (id == 'zcbm' || id == 'zcpd') {

        did = 'bslcjs';
    }

    $.ajax({
        type: "post",
        dataType: "json",
        //url: "https://hr.hbsrcfwj.cn/api/hr/board/page",
        url: "https://hr.hbsrcfwj.cn/api/hr/boardCat/page",
        headers: {
            "Content-Type": "application/json;charset=utf-8"
        },

        data: JSON.stringify({
            "query": {
                "cat": did,
            },
            "pageSize": 10,
            "pageNo": 1,
            "sort": "-_createTime",
            "fields": "title,titleImages,_createTime"

        }),
        //							data:JSON.stringify(),
        //			headers: { "Content-Type": "application/x-www-form-urlencoded" },
        //请求成功
        success: function(data) {
            if (data.code == 200) {
                console.log(data.result.records);
                switch (id) {
                    case 'wxhb':
                        getCat('wxhb', data.result.records[0].id);
                        break;
                    case 'xwzx':
                        newListHtml('xwzx', data.result.records);
                        break;
                    case 'zcfg':
                        newListHtml('zcfg', data.result.records);
                        break;
                    case 'gsxw':
                        newListHtml('gsxw', data.result.records);
                        break;
                    case 'zcfx':
                        newListHtml('zcfx', data.result.records);
                        break;
                    case 'szyw':
                        newListHtml('szyw', data.result.records);
                        break;
                    case 'djgzzt':
                        newListHtml('djgzzt', data.result.records);
                        break;
                    case 'dwbk':
                        newListHtml('dwbk', data.result.records);
                        break;
                    case 'xxyd':
                        newListHtml('xxyd', data.result.records);
                        break;
                    case "txl":
                        txlListHtml('txl', data.result.records);
                        break;
                    case "xxxd":
                        newListHtml('xxxd', data.result.records);
                        break;
                        //雇员服务/灵活用工
                    case "lhyg":
                        getCat('lhyg', data.result.records[0].id);
                        break;
                    case "rsdlfw":
                        getCat('rsdlfw', data.result.records[0].id);
                        break;
                    case "wbfw":
                        getCat('wbfw', data.result.records[0].id);
                        break;
                    case "lwpq":
                        getCat('lwpq', data.result.records[0].id);
                        break;
                        //雇员服务/招聘服务
                    case "zpfw":
                        getCat('zpfw', data.result.records[0].id);
                        break;
                    case "dddrpofw":
                        getCat('dddrpofw', data.result.records[0].id);
                        break;
                    case "zgdrclt":
                        getCat('zgdrclt', data.result.records[0].id);
                        break;
                    case "fxqdgl":
                        getCat('fxqdgl', data.result.records[0].id);
                        break;
                    case "xyzp":
                        getCat('xyzp', data.result.records[0].id);
                        break;
                    case "dzfw":
                        getCat('dzfw', data.result.records[0].id);
                        break;
                        //雇员服务/档案服务
                    case "dafw":
                        getCat('dafw', data.result.records[0].id);
                        break;
                    case "daszhfw":
                        getCat('daszhfw', data.result.records[0].id);
                        break;
                    case "dazlfw":
                        getCat('dazlfw', data.result.records[0].id);
                        break;

                        //雇员服务/培训服务页面
                    case "pxfwym":
                        getCat('pxfwym', data.result.records[0].id);
                        break;
                    case "pxxm":
                        getCat('pxxm', data.result.records[0].id);
                        break;
                    case "fwnrjlc":
                        getCat('fwnrjlc', data.result.records[0].id);
                        break;

                        //党建服务
                    case "zcbm":
                        getCat('zcbm', data.result.records[0].id);
                        break;
                    case "zcpd":
                        getCat('zcpd', data.result.records[0].id);
                        break;
                    case "bslcjs":
                        getCat('bslcjs', data.result.records[0].id);
                        break;

                    default:
                        1;
                }

                //console.log(data);
            }

        },
        error: function() {
            console.log('error')
        }
    })
}

//拼接页面二级分类列表
function newListHtml(id, data) {
    //	console.log(data);
    $("#" + id).empty();
    var _li = '';
    for (var i = 0; i < data.length; i++) {
        if (data[i]._createTime == undefined) {
            data[i]._createTime = '';
        }

        _li += '<li class=' + id + '"_li" data-id="' + data[i].id + '">' +
            '<a href="news.html?id=' + data[i].id + '"><span>' + data[i].title + '</span></a>' +
            '<span>' + data[i]._createTime + '</span>' +
            '</li>';
    }
    $("#" + id).append(_li)

}

//通讯录列表
function txlListHtml(id, data) {
    //	console.log(data);

    $("#" + id).empty();
    var _li = '';
    for (var i = 0; i < data.length; i++) {

        _li += '<div class="phonelistleft_include" ' + id + '"_li" data-id="' + data[i].id + '">' +
            '<i class="fa fa-user-circle-o"></i><span>柏苏强</span><i class="fa fa-mobile"></i><span>15802**8345</span>' +
            '</div>';
    }
    $("#" + id).append(_li)
}

//获取页面详细内容
function getCat(htId, id) {

    $.ajax({
        type: "get",
        dataType: "json",
        url: "https://hr.hbsrcfwj.cn/api/hr/board/" + id,
        headers: {
            "Content-Type": "application/json;charset=utf-8"
        },
        data: {},
        //请求成功
        success: function(data) {
            //			console.log(data);
            if (data.code == 200) {
                switch (htId) {
                    case 'wxhb':
                        bdHtml('wxhb', data.result);
                        break;
                    case 'xwzx_content':
                        newsHtml('xwzx_content', data.result);
                        break;
                    case 'lwpq':
                        newsGyHtml('lwpq', data.result);
                        break;
                    case 'lhyg':
                        newsGyHtml('lhyg', data.result);
                        break;
                    case 'rsdlfw':
                        newsGyHtml('rsdlfw', data.result);
                        break;
                    case 'wbfw':
                        newsGyHtml('wbfw', data.result);
                        break;
                    case 'dafw':
                        newsGyHtml('dafw', data.result);
                        break;
                    case 'daszhfw':
                        newsGyHtml('daszhfw', data.result);
                        break;
                    case 'dazlfw':
                        newsGyHtml('dazlfw', data.result);
                        break;
                    case 'zcbm':
                        newsGyHtml('zcbm', data.result);
                        break;
                    case 'zcpd':
                        newsGyHtml('zcpd', data.result);
                        break;
                    case 'bslcjs':
                        newsGyHtml('bslcjs', data.result);
                        break;
                    case 'zpfw':
                        newsGyHtml('zpfw', data.result);
                        break;
                    case 'dddrpofw':
                        newsGyHtml('dddrpofw', data.result);
                        break;
                    case 'zgdrclt':
                        newsGyHtml('zgdrclt', data.result);
                        break;
                    case 'pxfwym':
                        newsGyHtml('pxfwym', data.result);
                        break;
                    case 'pxxm':
                        newsGyHtml('pxxm', data.result);
                        break;
                    case 'fwnrjlc':
                        newsGyHtml('fwnrjlc', data.result);
                        break;
                    case 'fxqdgl':
                        newsGyHtml('fxqdgl', data.result);
                        break;
                    case 'xyzp':
                        newsGyHtml('xyzp', data.result);
                        break;
                    case 'dzfw':
                        newsGyHtml('dzfw', data.result);
                        break;

                    default:
                        1;

                }
            }
        },
        error: function() {
            console.log('error')
        }
    });

}

function getFile(id) {
    $.ajax({
        type: "get",
        dataType: "json",
        url: "http://api6.hbsrcfwj.cn/file/" + id,
        headers: {
            "Content-Type": "application/json;charset=utf-8"
        },
        data: {},
        //请求成功
        success: function(data) {
            console.log(data);
            if (data.code == 200) {
                //				console.log(data)
            }
        },
        error: function() {
            console.log('error')
        }
    });

}

//拼接轮换详细内容
function bdHtml(id, data) {
    var _html = '';
    console.log(data);
    for (var i = 0; i < 1; i++) {
        _html += '<li>' + data.content + '</li>'
    }

    $("#" + id).html(_html);

}

//拼接页面详细内容
function newsHtml(id, data) {
    var _html = '';
    console.log(data);
    if (data._createTime == undefined) {
        data._createTime = '';
    }
    _html += '<p class="news_maintitle">' + data.title + '</p>' +
        '<p class="news_smalltitle">发布时间:' + data._createTime + '</p>' + data.content;

    $("#" + id).html(_html);

}

//拼接页面雇员服务下详细内容
function newsGyHtml(id, data) {
    var _html = '';
    console.log(data);
    _html += data.content;

    $("#" + id).html(_html);

}

//getId();
//我选湖北
// getBoard('wxhb');
$('.nav_1').click(function() {
    //公司介绍
    getBoard('xwzx');
})
$('.nav_2').click(function() {
    //时政要闻
    getBoard('szyw');
})
$('.nav_3').click(function() {
    //灵活用工
    getBoard('lhyg');
})
$('.nav_4').click(function() {
    //职称评定
    getBoard('zcpd');
})

$('.nav_news2').click(function() {
    //政策法规
    getBoard('zcfg');
})
$('.nav_news3').click(function() {
    //公司新闻
    getBoard('gsxw');
})
$('.nav_news4').click(function() {
    //政策分享
    getBoard('zcfx');
})

//服务项目
$('.nav_news1').click(function() {
    getBoard('djgzzt');
})
$('.nav_news2').click(function() {
    getBoard('dwbk');
})
$('.nav_news3').click(function() {
    getBoard('xxyd');
})
$('.nav_miniservice4').click(function() {
    getBoard('xxxd');
})
$('.nav_news5').click(function() {
    getBoard('txl');
})

//雇员服务/灵活用工
$('.nav_minipay1').click(function() {
    getBoard('zpfw');
})
$('.nav_minipay2').click(function() {
    getBoard('dafw');
})
$('.nav_minipay3').click(function() {
        getBoard('pxfwym');
    })
    //getBoard('lhyg');

$('.nav_minipay_mini1').click(function() {
    getBoard('rsdlfw');
})
$('.nav_minipay_mini2').click(function() {
    getBoard('wbfw');
})
$('.nav_minipay_mini3').click(function() {
    getBoard('lwpq');
})

//雇员服务/招聘服务

$('.nav_minipay_mini4').click(function() {
    getBoard('dddrpofw');
})
$('.nav_minipay_mini5').click(function() {
    getBoard('zgdrclt');
})
$('.nav_minipay_mini6').click(function() {
    getBoard('fxqdgl');
})
$('.nav_minipay_mini7').click(function() {
    getBoard('xyzp');
})
$('.nav_minipay_mini8').click(function() {
        getBoard('dzfw');
    })
    //雇员服务/档案服务
    //getBoard('dafw');
$('.nav_minipay_mini9').click(function() {
    getBoard('daszhfw');
})
$('.nav_minipay_mini10').click(function() {
    getBoard('dazlfw');
})
$('.nav_minipay_mini11').click(function() {
    getBoard('fwnrjlc');
})

//党建服务/办事流程介绍
$('.nav_minipay_mini9').click(function() {
    getBoard('daszhfw');
})
$('.nav_minipay_mini10').click(function() {
    getBoard('dazlfw');
})
$('.nav_minipay_mini11').click(function() {
    getBoard('fwnrjlc');
})



$('.nav_jobmini1').click(function() {
    getBoard('zcbm');
})
$('.nav_jobmini2').click(function() {
        getBoard('bslcjs');
    })
    //getBoard('zcpd');

//培训服务页面
//getBoard('pxfwym');
//getBoard('pxxm');