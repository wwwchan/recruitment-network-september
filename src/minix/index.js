//导航栏切换
export default {
    data() {
        return {
            token: ''
        }
    },
    methods: {
        //判断token，token为空跳转到登陆
        getToken() {
            this.token = localStorage.getItem('token');
            if (!this.token) {
                this.$message.error("请先登录！");
                setTimeout(() => {
                    this.$router.push({
                        path: "/signin"
                    });
                }, 1000);
                return false;
            }
        },
        //退出登录
        toIndex(){
            localStorage.removeItem("token");
            this.$router.push({
                path: "/"
            });
        },
       getSimpleText(html) {
            html = html.replace(/<\/?[^>]*>/g, ''); //去除HTML tag
            html = html.replace(/[ | ]*\n/g, '\n'); //去除行尾空白
            html = html.replace(/\n[\s| | ]*\r/g, '\n'); //去除多余空行
            html = html.replace(/&nbsp;/ig, ''); //去掉&nbsp;
            html = html.replace(/\s/g, ''); //将空格去掉
            html = html.replace(/<\/?.+?>/g, "");
            html = html.replace(/ /g, ""); //dds为得到后的内容      
            return html;
        },
        //静态页面js样式修改
        getCss() {
            $('.companyNav_choose>li').click(function() {
                    $(this).addClass('navOn');
                    $(this).siblings().removeClass('navOn');
                })
                //pc端nav切换
            var navIcon = $('.companyNav_choose>li');
            for (var i = 0; i < navIcon.length; i++) {
                $(navIcon).click(function() {
                    var navi = $(this).index();
                    var navContent = $('.companyContainer>div');

                    $(navContent[navi]).css('display', 'block');
                    $(navContent[navi]).siblings().css('display', 'none');
                })
            }
            //wap端nav切换
            var wapnavIcon = $('.navbar-nav>li');
            for (var i = 0; i < navIcon.length; i++) {
                $(wapnavIcon).click(function() {
                    var wapnavi = $(this).index();
                    var wapnavContent = $('.companyContainer>div');

                    $(wapnavContent[wapnavi]).css('display', 'block');
                    $(wapnavContent[wapnavi]).siblings().css('display', 'none');
                })
            }
            //二级栏目切换--公司介绍
            var companyIcon = $('.nav_common_title_news>li');
            for (var i = 0; i < companyIcon.length; i++) {
                $(companyIcon).click(function() {
                    var compi = $(this).index();
                    var compContent = $('.all_nav_common_list_news>div');

                    $(compContent[compi]).css('display', 'block');
                    $(compContent[compi]).siblings().css('display', 'none');
                })
            }
            //二级栏目切换--服务项目
            var serviceIcon = $('.nav_common_title_service>li');
            for (var i = 0; i < serviceIcon.length; i++) {
                $(serviceIcon).click(function() {
                    var compi = $(this).index();
                    var serviceContent = $('.all_nav_common_list_service>div');

                    $(serviceContent[compi]).css('display', 'block');
                    $(serviceContent[compi]).siblings().css('display', 'none');
                })
            }
            //二级栏目切换--雇员服务
            var flexIcon = $('.nav_common_title_flex>li');
            for (var i = 0; i < flexIcon.length; i++) {
                $(flexIcon).hover(function() {
                    var compi = $(this).index();
                    console.log(compi);
                    $(flexIcon[compi]).find('p').click(function() {

                        var flexContent = $('.all_nav_common_list_flex>div');

                        $(flexContent[compi]).css('display', 'block');
                        $(flexContent[compi]).siblings().css('display', 'none');
                    })


                })
            }
            //二级栏目切换--党建服务
            var joinIcon = $('.nav_common_title_join>li');
            for (var i = 0; i < joinIcon.length; i++) {
                $(joinIcon).click(function() {
                    var compi = $(this).index();

                    var joinContent = $('.all_nav_common_list_join>div');

                    $(joinContent[compi]).css('display', 'block');
                    $(joinContent[compi]).siblings().css('display', 'none');
                })
            }
            //二级栏目切换--我的简历
            var myIcon = $('.nav_common_title_my>li');
            for (var i = 0; i < myIcon.length; i++) {
                $(myIcon).click(function() {
                    var compi = $(this).index();
                    var myContent = $('.all_nav_common_list_my>div');

                    $(myContent[compi]).css('display', 'block');
                    $(myContent[compi]).siblings().css('display', 'none');
                })
            }
            //三级栏目--雇员服务--灵活用工
            var techminiIcon = $('.service_mini1>strong');
            for (var i = 0; i < techminiIcon.length; i++) {
                $(techminiIcon).click(function() {
                    $('.nav_common_list_flex_1').css('display', 'none');
                    $('.all_nav_commonmini_list_1').css('display', 'block');
                    $('.all_nav_commonmini_list_2').css('display', 'none');
                    $('.all_nav_commonmini_list_3').css('display', 'none');
                    $('.all_nav_commonmini_list_4').css('display', 'none');


                    var minicompi = $(this).index();
                    var techminiContent = $('.all_nav_commonmini_list_1>div');

                    $(techminiContent[minicompi]).css('display', 'block');
                    $(techminiContent[minicompi]).siblings().css('display', 'none');
                })
            }
            //三级栏目--雇员服务--招聘服务
            var techminiIcon1 = $('.service_mini2>strong');
            for (var i = 0; i < techminiIcon1.length; i++) {
                $(techminiIcon1).click(function() {
                    $('.nav_common_list_flex_2').css('display', 'none');
                    $('.all_nav_commonmini_list_2').css('display', 'block');
                    $('.all_nav_commonmini_list_1').css('display', 'none');
                    $('.all_nav_commonmini_list_3').css('display', 'none');
                    $('.all_nav_commonmini_list_4').css('display', 'none');
                    var compi = $(this).index();
                    var techminiContent1 = $('.all_nav_commonmini_list_2>div');

                    $(techminiContent1[compi]).css('display', 'block');
                    $(techminiContent1[compi]).siblings().css('display', 'none');
                })
            }
            //三级栏目--雇员服务--档案服务
            var techminiIcon2 = $('.service_mini3>strong');
            for (var i = 0; i < techminiIcon2.length; i++) {
                $(techminiIcon2).click(function() {
                    $('.nav_common_list_flex_3').css('display', 'none');
                    $('.all_nav_commonmini_list_3').css('display', 'block');
                    $('.all_nav_commonmini_list_2').css('display', 'none');
                    $('.all_nav_commonmini_list_1').css('display', 'none');
                    $('.all_nav_commonmini_list_4').css('display', 'none');
                    var compi = $(this).index();
                    var techminiContent2 = $('.all_nav_commonmini_list_3>div');

                    $(techminiContent2[compi]).css('display', 'block');
                    $(techminiContent2[compi]).siblings().css('display', 'none');
                })
            }
            //三级栏目--雇员服务--培训服务页面
            var techminiIcon3 = $('.service_mini4>strong');
            for (var i = 0; i < techminiIcon3.length; i++) {
                $(techminiIcon3).click(function() {
                    $('.nav_common_list_flex_4').css('display', 'none');
                    $('.all_nav_commonmini_list_4').css('display', 'block');
                    $('.all_nav_commonmini_list_2').css('display', 'none');
                    $('.all_nav_commonmini_list_3').css('display', 'none');
                    $('.all_nav_commonmini_list_1').css('display', 'none');
                    var compi = $(this).index();
                    var techminiContent3 = $('.all_nav_commonmini_list_4>div');

                    $(techminiContent3[compi]).css('display', 'block');
                    $(techminiContent3[compi]).siblings().css('display', 'none');
                })
            }
            var newtop;
            $('.process_1').click(function() {
                $('.serviceContainer').css('display', 'block')
                $('.serviceContainer').siblings().css('display', 'none')
                    //滑到顶部
                var timer = setInterval(function() {
                    newtop = $(document).scrollTop();
                    if ($(document).scrollTop() == 0) {
                        clearInterval(timer);
                    } else {
                        var a = $(document).scrollTop(newtop - 20);
                    }
                }, 10);
            })
            $('.process_3').click(function() {
                $('.employeeService').css('display', 'block')
                $('.employeeService').siblings().css('display', 'none')
                    //滑到顶部
                var timer = setInterval(function() {
                    newtop = $(document).scrollTop();
                    if ($(document).scrollTop() == 0) {
                        clearInterval(timer);
                    } else {
                        var a = $(document).scrollTop(newtop - 20);
                    }
                }, 10);
            })


            $('.service_minicommon>strong').hover(function() {
                    $(this).children('a').css('color', 'rgb(229,1,19)');
                    $(this).children().children('span').css('color', 'rgb(229,1,19)');
                }, function() {
                    $(this).children('a').css('color', 'rgb(51,51,51)');
                    $(this).children().children('span').css('color', 'rgb(51,51,51)');
                })
                //雇员服务end


            //公司介绍切换
            $('.nav_common_title>li').hover(function() {
                    $(this).addClass('titleOn');
                    $(this).siblings().removeClass('titleOn');
                    $(this).find('a').find('p').addClass('borderLeft');
                    $(this).siblings().find('a').find('p').removeClass('borderLeft');
                    $(this).css({
                        'font-weight': 'bold',
                        'text-indent': '2rem'
                    });
                }, function() {
                    $(this).css({
                        'font-weight': 'normal',
                        'text-indent': '2.5rem'
                    });
                })
                //通讯录切换
            $('.phonelist_right>li').hover(function() {
                    $(this).addClass('phoneOn');
                    $(this).siblings().removeClass('phoneOn');
                })
                //首页左右箭头悬浮变色
            $('.prev').hover(function() {
                $(this).css('background-image', 'url("../assets/images/next.png")');
            }, function() {
                $(this).css('background-image', 'url("../assets/images/prevBefore.png")');
            })
            $('.next').hover(function() {
                    $(this).css('background-image', 'url("../assets/images/next.png")');
                }, function() {
                    $(this).css('background-image', 'url("../assets/images/nextBefore.png")');
                })
                //首页招聘悬浮变色
            $('.serviceSpecial__common_1').hover(function() {
                $(this).hasClass('red') ? $(this).removeClass('red').addClass('green') : $(this).removeClass('green').addClass('red');
            })
            $('.serviceSpecial__common_2').hover(function() {
                    $(this).hasClass('red') ? $(this).removeClass('red').addClass('green') : $(this).removeClass('green').addClass('red');

                })
                //党建服务悬浮效果
            $('.sixLinkto>a').hover(function() {
                $(this).addClass('linkOn');
                $(this).siblings().removeClass('linkOn');
                $(this).find('i').hasClass('linkBefore') ? $(this).find('i').removeClass('linkBefore').addClass('linkWhite') : $(this).find('i').removeClass('linkWhite').addClass('linkBefore');
            })
            $('.linkSecond').hover(function() {
                $('.linkFirst').find('i').removeClass('linkSpecial');
            })
            $('.linkThird').hover(function() {
                $('.linkFirst').find('i').removeClass('linkSpecial');
            })
            $('.linkForth').hover(function() {
                $('.linkFirst').find('i').removeClass('linkSpecial');
            })
            $('.linkFifth').hover(function() {
                $('.linkFirst').find('i').removeClass('linkSpecial');
            })
            $('.linkSixth').hover(function() {
                    $('.linkFirst').find('i').removeClass('linkSpecial');
                })
                // 手机端显示隐藏
            $('.wapShow').click(function() {
                $('.navbar-collapse').slideToggle(400);
            })
            $('.navbar-nav>li').hover(function() {
                    $(this).addClass('navOn');
                    $(this).siblings().removeClass('navOn');
                })
                //我要报名
            $('.mysignup').click(function() {
                    $('.signupWorkplace').show();
                })
                //关闭弹窗
            $('.signupplace_save').click(function() {
                $('.signupWorkplace').hide();
            })
            $('.fa-times').click(function() {
                    $('.signupWorkplace').hide();
                })
                //公司新闻-文章页
            $('.news').click(function() {
                $('.newsContainer').hide();
                $('.newsPresent').show();
                $('.nav_common_title_news').show()
                $('.newsPresentcompany').hide();
                $('.all_nav_common_list').show();
            })

            $('.news_enter').click(function() {
                    $('.nav_common_title_news').hide();
                    $('.newsPresent').hide();
                    $('.newsContainer').show()
                    $('.newsPresentcompany').hide();
                    $('.all_nav_common_list').hide();
                })
                //手机端一开始去掉左边栏目横杠
            var w = $(window).width();
            if (w < 768) {
                $('.newsPre').find('p').removeClass('borderLeft');

            }
            //我的面试选项切换
            $('.my_meeting').click(function() {
                $('.mymeeting_1').show();
                $('.mymeeting_2').hide();
                $('.mymeeting_3').hide();
                $('.mymeeting_4').hide();
                $('.mymeeting_5').hide();
               
            })
            $('.person_upload').click(function() {
                $('.mymeeting_2').show();
                $('.mymeeting_1').hide();
                $('.mymeeting_3').hide();
                $('.mymeeting_4').hide();
                $('.mymeeting_5').hide();
                $('.mymeeting_6').hide();
            })
            $('.account_set').click(function() {
                $('.mymeeting_3').show();
                $('.mymeeting_1').hide();
                $('.mymeeting_2').hide();
                $('.mymeeting_4').hide();
                $('.mymeeting_5').hide();
                $('.mymeeting_6').hide();
            })
             //我的投递选项切换
             $('.mytd').click(function() {
                $('.mymeeting_1').hide();
                $('.mymeeting_2').hide();
                $('.mymeeting_3').hide();
                $('.mymeeting_4').show();
                $('.mymeeting_5').hide();
                $('.mymeeting_6').hide();
                // $('.mymeeting_2').hide();
                // $('.mymeeting_3').hide();
            })
             //我的报名切换
             $('.mybm').click(function() {
                $('.mymeeting_1').hide();
                $('.mymeeting_2').hide();
                $('.mymeeting_3').hide();
                $('.mymeeting_4').hide();
                $('.mymeeting_5').hide();
                $('.mymeeting_6').show();
                // $('.mymeeting_2').hide();
                // $('.mymeeting_3').hide();
            })
            //我的简历
              //我的投递选项切换
              $('.myjl').click(function() {
                $('.mymeeting_1').hide();
                $('.mymeeting_2').hide();
                $('.mymeeting_3').hide();
                $('.mymeeting_4').hide();
                $('.mymeeting_5').show();
                $('.mymeeting_6').hide();
                // $('.mymeeting_2').hide();
                // $('.mymeeting_3').hide();
            })

            //header上选择变色
            $(".mainHeader_middle>li").click(function() {
                $(this).addClass("middleOn");
                $(this).siblings().removeClass("middleOn");
            });

            //我的简历保存/取消
            $(".jobResume_save>button").hover(function() {
                $(this).addClass("saveOn");
                $(this).siblings().removeClass("saveOn");
            });
            //联系方式修改
            $(".resetNumber").click(function() {
                $("input[name='number']").val("");
            });
            //电子邮箱修改
            $(".resetMail").click(function() {
                $("input[name='mail']").val("");
            });
            //关闭弹窗
            $(".expectplace_save").click(function() {
                $(".expectWorkplace").hide();
            });
            $(".fa-times").click(function() {
                $(".expectWorkplace").hide();
            });

            //点击切换期望工作地点
            var expectIcon = $(".expectplace_common input");
            for (var i = 0; i < expectIcon.length; i++) {
                $(expectIcon).click(function() {
                    var work_choose = $(this).val();
                    $(".workplace_choose").text(work_choose);
                });
            }
            //登录

            var w = $(window).width();
            if (w < 767) {
                $('.signinPage_logo img').attr("src", "../assets/images/banner4.png");
            }
            var w = $(window).width();
            if (w < 767) {
                $('.signphonePage_logo img').attr("src", "assets/images/banner4.png");
            }
            //登录方式切换
            $('.signin_password').click(function() {
                $('.signinPage').css('display', 'block');
                $('.signphonePage').css('display', 'none');
            })
            $('.signin_phone').click(function() {
                    $('.signinPage').css('display', 'none');
                    $('.signphonePage').css('display', 'block');
                })
                //点击注册图片变化
            var w = $(window).width();
            if (w < 767) {
                $('.registerPage_logo img').attr("src", "assets/images/banner4.png");
            }
            //header上选择变色
            $('.mainHeader_middle>li').click(function() {
                    $(this).addClass('middleOn');
                    $(this).siblings().removeClass('middleOn');
                })
                //重置密码
            var w = $(window).width();
            if (w < 767) {
                $('.repasswordPage_logo img').attr("src", "assets/images/banner4.png");
            }
            //job
            $('.mainHeader_middle>li').click(function() {
                $(this).addClass('middleOn');
                $(this).siblings().removeClass('middleOn');
            })

            
            //雇员服务子菜单
            $('.service_1').click(function() {
                $('.service_mini1').toggle();
                $('.techProcess').hide();
            })

            $('.service_2').click(function() {
                $('.service_mini2').toggle();
            })
            $('.service_3').click(function() {
                $('.service_mini3').toggle();
            })
            $('.service_4').click(function() {
                $('.service_mini4').toggle();
            })
        }
    }
}