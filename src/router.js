import Vue from 'vue';
import Router from 'vue-router';


Vue.use(Router);

const routers = new Router({
    routes: [{
            path: '/',
            name: 'index',
            component: resolve => (require(["@/views/index"], resolve))
        },
        {
            path: '/list',
            name: 'list',
            component: resolve => (require(["@/views/List"], resolve))
        },
        {
            path: '/newsDetail',
            name: 'newsDetail',
            component: resolve => (require(["@/views/index/newsDetail"], resolve))
        },
        {
            path: '/gsjs',
            name: 'gsjs',
            component: resolve => (require(["@/views/index/gsjs"], resolve))
        },
        {
            path: '/company',
            name: 'company',
            component: resolve => (require(["@/views/company"], resolve))
        },
        {
            path: '/company_detail',
            name: 'company_detail',
            component: resolve => (require(["@/views/company_detail"], resolve))
        },
        {
            path: '/editresume',
            name: 'editresume',
            component: resolve => (require(["@/views/editresume"], resolve))
        },
        {
            path: '/job',
            name: 'job',
            component: resolve => (require(["@/views/job"], resolve))
        },
        {
            path: '/job_detail',
            name: 'job_detail',
            component: resolve => (require(["@/views/job_detail"], resolve))
        },
        {
            path: '/news',
            name: 'news',
            component: resolve => (require(["@/views/news"], resolve))
        },
        {
            path: '/person',
            name: 'person',
            component: resolve => (require(["@/views/person"], resolve))
        },
        {
            path: '/register',
            name: 'register',
            component: resolve => (require(["@/views/register"], resolve))
        },
        {
            path: '/reset_password',
            name: 'reset_password',
            component: resolve => (require(["@/views/reset_password"], resolve))
        },
        {
            path: '/resume',
            name: 'resume',
            component: resolve => (require(["@/views/resume"], resolve))
        },
        {
            path: '/signin',
            name: 'signin',
            component: resolve => (require(["@/views/signin"], resolve))
        }
    ],
    // 处理路由中的锚点
    scrollBehavior(to, from, savedPosition) {
        if (to.hash) {
            return { selector: to.hash };
        }
    }
});




export default routers;