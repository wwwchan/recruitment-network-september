/**
 * vue-cli rc3.0配置项
 * https://cli.vuejs.org/config
 */
const path = require('path');

module.exports = {
    // 应用程序将部署的base URL
    publicPath: './',
    // 编译打包后的输出目录
    outputDir: 'dist',
    // 编译打包后的静态资源目录
    assetsDir: 'assets',
    // 应用程序入口文件生成配置，支持多文件，至少包含应用入口
    pages: {
        index: {
            // 应用入口
            entry: 'src/main.js',
            // 模板文件
            template: 'public/index.html',
            // 应用程序入口
            filename: 'index.html'
        }
    },
    // souce map
    productionSourceMap: false,
    // merge webpack config
    configureWebpack: {
        resolve: {
            extensions: ['.js', '.json', '.vue', '.scss', '.css'],
            alias: {
                '~P': path.join(__dirname, 'public'), // ./public
                '@': path.join(__dirname, 'src'), // ./src
                '~V': path.join(__dirname, 'src/views'), // ./src/views
                '~C': path.join(__dirname, 'src/components'), // ./src/components
                '~A': path.join(__dirname, 'src/assets') // ./src/assets
            }
        }
    },
    // 本地服务
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        host: '0.0.0.0',
        port: 4096,
        hot: true,
        open: true,
        // 代理配置
        proxy: {
            '/tc_web': {
                target: 'http://192.168.4.49:9999',
                ws: false,
                sesure: true,
                changeOrigin: true,
                pathRewrite: {
                    '^/tc_web': '/tc_web'
                }
            },
            '/tc_web': {
                target: 'http://120.224.63.201:8082',
                ws: false,
                sesure: true,
                changeOrigin: true,
                pathRewrite: {
                    '^/tc_web': '/tc_web'
                }
            }
        }
    }
};